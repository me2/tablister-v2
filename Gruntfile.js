module.exports = function (grunt) {
  // dev builds
  grunt.registerTask('dev', ['less:all', 'copy:all', 'uglify:site', 'curl:bundle', 'uglify:bundle'])
  // for a dev build with files copied to /review for Firefox review submission
  grunt.registerTask('review', ['dev', 'copy:review'])
  // for a dev build that updates version #s from package.json
  grunt.registerTask('default', ['dev', 'replace'])
  // watcher
  grunt.registerTask('watcher', ['dev', 'concurrent:all'])

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    less: {
      all: {
        options: {
          paths: ['/'],
          rootpath: '',
          compress: true,
          cleancss: true
        },
        files: {
          './dist/tablister.web-ext/css/display.css': './less/display.less',
          './assets/css/display.css': './less/display.less',
          './assets/css/site.css': './less/site.less'
        }
      }
    },

    copy: {
      all: {
        files: [
          {
            expand: true,
            cwd: 'node_modules/clipboard/dist',
            src: 'clipboard.min.js',
            dest: './assets/'
          },
          {
            expand: true,
            cwd: 'node_modules/ionicons/css',
            src: 'ionicons.css',
            dest: './assets/css/'
          },
          {
            expand: true,
            cwd: 'node_modules/ionicons/fonts',
            src: 'ionicons.ttf',
            dest: './assets/fonts/'
          },
          {
            expand: true,
            cwd: 'node_modules/ionicons/fonts',
            src: 'ionicons.woff',
            dest: './assets/fonts/'
          },

          {
            expand: true,
            cwd: 'assets/images',
            src: 'Icon*.png',
            dest: './dist/tablister.web-ext/Icon/'
          },
          {
            expand: true,
            cwd: 'node_modules/clipboard/dist',
            src: 'clipboard.min.js',
            dest: './dist/tablister.web-ext/'
          },
          {
            expand: true,
            cwd: 'node_modules/ionicons/css',
            src: 'ionicons.css',
            dest: './dist/tablister.web-ext/css/'
          },
          {
            expand: true,
            cwd: 'node_modules/ionicons/fonts',
            src: 'ionicons.ttf',
            dest: './dist/tablister.web-ext/fonts/'
          },
          {
            expand: true,
            cwd: 'node_modules/ionicons/fonts',
            src: 'ionicons.woff',
            dest: './dist/tablister.web-ext/fonts/'
          }
        ]
      },

      review: {
        files: [
          {
            expand: true,
            cwd: 'dist/tablister.web-ext',
            src: '**/*',
            dest: './review/dist/tablister.web-ext/'
          },
          {
            expand: true,
            cwd: 'less',
            src: '**/*',
            dest: './review/less/'
          },
          {
            expand: true,
            cwd: 'src',
            src: '**/*',
            dest: './review/src/'
          },
          {
            expand: true,
            cwd: 'test',
            src: '**/*',
            dest: './review/test/'
          },
          {
            src: 'Build.md',
            dest: './review/Build.md'
          },
          {
            src: 'Gruntfile.js',
            dest: './review/Gruntfile.js'
          },
          {
            src: 'package.json',
            dest: './review/package.json'
          },
          {
            src: 'webpack.config.js',
            dest: './review/webpack.config.js'
          }
        ]
      }
    },

    curl: {
      'bundle': {
        src: 'http://localhost:8080/bundle.js',
        dest: 'bundle.js'
      }
    },

    uglify: {
      site: {
        options: {
          preserveComments: false
        },
        files: {
          './assets/site.min.js': [
            './assets/site.js'
          ]
        }
      },
      bundle: {
        options: {
          preserveComments: false
        },
        files: {
          './assets/bundle.min.js': [
            './bundle.js'
          ],
          './dist/tablister.web-ext/bundle.min.js': [
            './bundle.js'
          ]
        }
      }
    },

    replace: {
      build_replace: {
        options: {
          variables: {
            'version': '<%= pkg.version %>'
          }
        },
        // Source and destination files
        files: [
          {
            src: ['index.@@replace.html'],
            dest: 'index.html'
          },
          {
            src: ['save/view-v2.@@replace.php'],
            dest: 'save/view-v2.php'
          },
          {
            src: ['dist/tablister.web-ext/display-newtab.@@replace.html'],
            dest: 'dist/tablister.web-ext/display-newtab.html'
          },
          {
            src: ['dist/tablister.web-ext/display-popup.@@replace.html'],
            dest: 'dist/tablister.web-ext/display-popup.html'
          },
          {
            src: ['dist/tablister.web-ext/display.@@replace.html'],
            dest: 'dist/tablister.web-ext/display.html'
          },
          {
            src: ['dist/tablister.web-ext/manifest.@@replace.json'],
            dest: 'dist/tablister.web-ext/manifest.json'
          }
        ]
      }
    },

    zip: {
      'long-format': {
        expand: true,
        // cwd: 'review',
        src: './review/**/*',
        dest: 'tablister-review.<%= pkg.version %>.zip'
      }
    },

    watch: {
      scripts: {
        files: 'src/**/*.js',
        tasks: ['curl:bundle', 'uglify:bundle']
      },
      styles: {
        files: 'less/**/*.less',
        tasks: ['less:all']
      }
    },

    concurrent: {
      options: {
        logConcurrentOutput: true
      },
      all: {
        tasks: ["watch:scripts", "watch:styles"]
      }
    }

  })

  grunt.loadNpmTasks('grunt-concurrent')
  grunt.loadNpmTasks('grunt-contrib-copy')
  grunt.loadNpmTasks('grunt-contrib-less')
  grunt.loadNpmTasks('grunt-contrib-uglify')
  grunt.loadNpmTasks('grunt-contrib-watch')
  grunt.loadNpmTasks('grunt-curl')
  grunt.loadNpmTasks('grunt-replace')
  grunt.loadNpmTasks('grunt-zip')
}
