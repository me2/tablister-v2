<?php
/*
**  This file is exactly like /display.html
**
**  EXCEPT, a query string param populates window.tablisterJson
**  based on mod rewrites (and a clean url)
**
**  This should get rewritten (maybe as an HBS template) so the the html`
**  for the extension and thesite's display of saved webpages is directly shared
*/
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="robots" content="noindex" />
    <meta name="viewport" content="width=750, initial-scale=0.5, minimum-scale=0.5, maximum-scale=0.5" />
    <title>Tab Lister</title>
    <link rel="stylesheet" type="text/css" href="/assets/display.css">

    <script> window.tablisterJson = '<?php echo $_GET['key']; ?>'; </script>

</head>

<body>
    <header>
        <h1><a href="http://tablister.com"><img src="/assets/images/Icon-32.png" alt="" class="logo"> TabLister</a></h1>
    </header>
    <main>
        <section id="displayLinks">
            <div class="container"></div>
        </section>
    </main>
    <footer>
        <p class="get-tablister not-installed"><a href="http://tablister.com">Get TabLister</a></p>
        Built by Paul Griffin Petty, 2015
    </footer>
</body>

<script src="/bundle.min.js"></script>
</html>

