<!DOCTYPE html>
<html lang="en">

<head>

    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=750, initial-scale=0.5, minimum-scale=0.5, maximum-scale=0.5" />
    <title>Tab Lister</title>
    <link rel="stylesheet" type="text/css" href="/assets/display.css">

<?php
if ($_GET['wid']) {
?>

    <style>
        .windows_links h4 span {display:none;}
        .windows_links h4 span.viewWindowName {display:block;}
        #linkHtml {list-style-type:none;margin:0;padding:0;}
        #linkHtml>li {margin:0;padding:0;}
    </style>

<?php
}
?>

    <link rel="apple-touch-icon" sizes="57x57" href="/assets/favicon/apple-touch-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/assets/favicon/apple-touch-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/assets/favicon/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/assets/favicon/apple-touch-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/assets/favicon/apple-touch-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/assets/favicon/apple-touch-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/assets/favicon/apple-touch-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/assets/favicon/apple-touch-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/assets/favicon/apple-touch-icon-180x180.png">
    <link rel="icon" type="image/png" href="/assets/favicon/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="/assets/favicon/android-chrome-192x192.png" sizes="192x192">
    <link rel="icon" type="image/png" href="/assets/favicon/favicon-96x96.png" sizes="96x96">
    <link rel="icon" type="image/png" href="/assets/favicon/favicon-16x16.png" sizes="16x16">
    <link rel="manifest" href="/assets/favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="msapplication-TileImage" content="/assets/favicon/mstile-144x144.png">
    <meta name="theme-color" content="#ffffff">

</head>

<body>
    <header>

        <h1><a href="http://tablister.com"><img src="/assets/images/Icon-32.png" alt="" class="logo"> TabLister</a></h1>

    </header>
    <nav style="display:none">

        <a class="restoreAll ion-ios-paper-outline color-blue"><span>open all links</span></a>

        <a class="mailAll ion-ios-email-outline color-blue"><span>email all links</span></a>

        <a class="exportAll ion-ios-paperplane-outline color-blue"><span>export all links</span></a>

    </nav>
    <main>

        <section id="displayLinks">

            <ol id="linkHtml">
            </ol>

        </section>

        <form id="exportLinks" style="display:none">
            <a class="copyExportAll color-blue" data-clipboard-target="#exportAll">copy text below:</a>
            <a class="exportAll color-red ion-ios-close-outline float-right">close</a>
            <textarea id="exportAll"></textarea>
        </form>

    </main>
    <footer>

        <p class="get-tablister not-installed"><a href="http://tablister.com">Get TabLister</a></p>

        Built by Paul Griffin Petty, 2015

    </footer>
</body>

<script type="text/javascript" charset="utf-8" src="/assets/site.min.js"></script>
<script type="text/javascript">

    var json = <?php include "cache/".$_GET['key']; ?>;

<?php
if ($_GET['wid']) {
?>

    var wid = "<?php echo $_GET['wid']; ?>";

    tlSite.loadWidByKey(json, wid);

<?php
} else {
?>

    tlSite.loadAllWids(json);

<?
}
?>

</script>

</html>

