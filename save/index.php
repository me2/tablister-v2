<?php

header('Access-Control-Allow-Origin: *'); 

$key = uniqid();

$fh = fopen("cache/" . $key, 'w') or die("Error");

fwrite($fh, json_encode($_POST['json']));

fclose($fh);

echo $key;

