<!DOCTYPE html>
<html lang="en">

<head>

    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="robots" content="noindex" />
    <meta name="viewport" content="width=750, initial-scale=0.5, minimum-scale=0.5, maximum-scale=0.5" />
    <title>Tab Lister</title>
    <link rel="stylesheet" type="text/css" href="/assets/css/display.css?version=5.4.1">
    <link rel="stylesheet" type="text/css" href="/assets/css/ionicons.css?version=5.4.1">

	<style>
		.toolbar:not(.toolbar-site),
		.toggleSettings,
		.tab-saved,
		.tab-captured > h3,
		.toolbar-site > div:last-child,
		.toolbar-site ~ .TablisterLoading { display: none !important; }
    </style>

    <script> window.tablisterJson = "<?php echo $_GET['key']; ?>"; </script>

</head>

<body>
    <header>
        <h1><a href="https://tablister.com" target="_new"><img src="/assets/images/Icon-32.png" alt="" class="logo"> TabLister </a></h1>
    </header>
    <main>
        <section class="container">
            <div class="TablisterLoading"><span class='ion-load-c'></span>Loading&#8230;</div>
        </section>
    </main>
    <footer>
        <p class="get-tablister installed">Thanks for using TabLister, 5.4.1</p>
        Built by Paul Griffin Petty, 2015 <span class="yyyy"></span>
    </footer>
</body>

<script src="/assets/clipboard.min.js?version=5.4.1"></script>
<script src="/assets/bundle.min.js?version=5.4.1"></script>
<script> document.querySelector('.yyyy').innerHTML = ' &mdash; ' + (new Date()).getFullYear() </script>

</html>
