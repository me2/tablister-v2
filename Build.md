
### First (if creating a release):

Update latest version number in package.json

To build, run:

    npm install ## optionally
    npm start
    grunt

### For Chrome:

Zip the dist/tablister.web-ext dir, so:

    cd dist/ && zip -r tab_lister_2.0.version.zip tablister.web-ext && cd ../ \
    && mv dist/tab_lister_2.0.version.zip ./tab_lister_2.0.version.zip

### For Firefox:

From within the dist/tablister.web-ext dir, from iTerm, so:

    cd dist/tablister.web-ext && web-ext build && cd ../../

... that zip is the extension.

For code review, submit this zip:

    grunt review && zip -r tab_lister-review.version.zip review

... lastly, cut-n-paste this file's contents, for Build Instructions during submission.

## OR, Run Release Script:

`sh Release.sh 6.6.6` where 6.6.6 is the new version number,

NOTE: You must update the version # in package.json & pass as an arg to to Release.sh

When done the script should generate a diff that looks like this:

Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git checkout -- <file>..." to discard changes in working directory)

	modified:   dist/tablister.web-ext/display-newtab.html
	modified:   dist/tablister.web-ext/display-popup.html
	modified:   dist/tablister.web-ext/display.html
	modified:   dist/tablister.web-ext/manifest.json
	modified:   index.html
	modified:   package.json
	modified:   save/view-v2.php

Untracked files:
  (use "git add <file>..." to include in what will be committed)

	tab_lister_2.0-6.6.6.chrome.zip
	tab_lister_2.0-6.6.6.firefox.zip
	tab_lister_2.0-6.6.6.review.zip

### Browser Sites:

https://chrome.google.com/webstore/developer/dashboard/g09049130400691788292?authuser=1

https://addons.mozilla.org/en-US/developers/
