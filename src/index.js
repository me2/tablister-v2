import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import _ from 'lodash'
import fetch from 'isomorphic-fetch'
import store from 'store'
import Flickity from '../node_modules/flickity/js/flickity.js'

import AlertBanner from './components/alert-banner'
import SiteToolbar from './components/toolbar-site'
import Settings from './components/settings'
import WindowsCaptured from './components/windows-captured'
import WindowsSaved from './components/windows-saved'
import WindowsSavedWebPages from './components/windows-saved-webpages'
import ExportTabs from './components/export-tabs'

class TablisterApp extends Component {
  constructor(props) {
    super(props)

    const detect = function () {
      let ua = navigator.userAgent
      let temp = null
      let M = ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || []

      if (/trident/i.test(M[1])) {
        temp = /\brv[ :]+(\d+)/g.exec(ua) || []
        return 'IE ' + (temp[1] || '')
      }

      if (M[1] === 'Chrome') {
        temp = ua.match(/\b(OPR|Edge)\/(\d+)/)
        if (temp != null) return temp.slice(1).join(' ').replace('OPR', 'Opera')
      }

      M = M[2] ? [M[1], M[2]] : [navigator.appName, navigator.appVersion, '-?']

      if ((temp = ua.match(/version\/(\d+)/i)) != null) M.splice(1, 1, temp[1])

      return M[0] // M.join(' ')
    }

    const env = { browser: null, platform: null }

    env.browser = detect().toLowerCase()

    chrome.runtime.getPlatformInfo(function (platform) {
      env.platform = platform
    })

    env.newTabUrl = (env.browser === 'chrome' ? 'chrome://newtab/' : 'about:newtab')

    document.querySelector('.yyyy').innerHTML = ' &mdash; ' + (new Date()).getFullYear()

    if (document.querySelector('.originalNewTab')) {
      document.querySelector('.originalNewTab').addEventListener('click', () => {
        chrome.tabs.update({
          url: env.newTabUrl
        })
      })
    }

    let loadedSettings = this.getSettings()

    this.state = {
      context: 'extension',
      settings: loadedSettings,
      cacheId: window.tablisterJson,
      windows: null,
      savedWindows: null,
      savedWebPages: null,
      windowsCount: 0,
      savedWindowsCount: 0,
      savedWebPagesCount: 0,
      environment: env,
      pluginId: chrome.runtime.id,
      pluginUrl: window.location.href,
      config: {
        saveBase: 'https://tablister.com/save-v2/',
        viewBase: 'https://tablister.com/view-v2/',
        ajaxBase: 'https://tablister.com/save/cache/'
      }
    }

    // this is for the webpage UI
    if (this.state.cacheId) {
      this.loadFromSaveCache(this.state.cacheId)
      this.state.context = 'site'
    } else {
      this.displayExtensionInfo()
      this.captureWindowTabs(this.state.settings.applyToAllWindows, this.state.settings.closeTabs)
      this.state.context = 'extension'
    }

    this.copyExportAll()
    this.checkThemeSetting()
    this.watchWindows()

    window.onload = () => {
      window.f = new Flickity('.carousel', {
        adaptiveHeight: true,
        percentPosition: false,
        cellSelector: '.carousel-cell'
      })

      window.f.on('select', (index) => {
        document.querySelectorAll('.toolbar-site button').forEach((el) => {
          el.classList.remove('selected')
        })
        document.querySelectorAll('.toolbar-site button')[index].classList.add('selected')
      })

      setTimeout(() => {
        window.f.dispatchEvent('settle')
      }, 200)

      window.f.on('settle', () => {
        window.f.resize()
      })

      window.addEventListener('resize', this.debounce(() => {
        window.f.resize()
      }, 100))
    }
  }

  displayExtensionInfo() {
    fetch('./manifest.json', {
      method: 'get',
      headers: { 'Content-Type': 'application/json' }
    }).then(response => response.json())
      .then((data) => {
        console.log(`%c${data.name} ${data.version}`, 'color: red; font-size: 110%;')
      })
  }

  getSettings() {
    let loadedSettings = store.get('tabListerOptions')

    if (typeof loadedSettings === 'undefined') {
      loadedSettings = {
        applyToAllWindows: false,
        closeTabs: false,
        useDarkTheme: false,
        replaceNewTab: false
      }

      store.set('tabListerOptions', loadedSettings)
    }

    return loadedSettings
  }

  updateSettings(setting) {
    console.log('updateSettings', setting)

    document.getElementById('settings').setAttribute('data-throttle-clicks', true)

    let settings = null
    let settingDisplayName = null

    if (setting === 'apply-to-all-windows') {
      settings = {
        applyToAllWindows: !this.state.settings.applyToAllWindows
      }

      settingDisplayName = 'Apply to All Windows'
    }

    if (setting === 'close-tabs') {
      settings = {
        closeTabs: !this.state.settings.closeTabs
      }

      settingDisplayName = 'Close Windows & Tabs'
    }

    if (setting === 'use-dark-theme') {
      settings = {
        useDarkTheme: !this.state.settings.useDarkTheme
      }

      settingDisplayName = 'Use Dark Theme'
    }

    if (setting === 'replace-newtab') {
      settings = {
        replaceNewTab: !this.state.settings.replaceNewTab
      }

      settingDisplayName = 'Replace New Tab'
    }

    function extend(obj, src) {
      Object.keys(src).forEach(function (key) { obj[key] = src[key]; })
      return obj
    }

    let UpdatedSettings = extend(this.state.settings, settings)

    this.setState({ settings: UpdatedSettings },
      () => {
        store.set('tabListerOptions', UpdatedSettings)
        document.getElementById('settings').setAttribute('data-throttle-clicks', false)
      })

    this.checkThemeSetting()

    this.alert(`Setting: ${settingDisplayName} updated`, 'success')
  }

  checkThemeSetting() {
    // console.log('useDarkTheme', this.state.settings.useDarkTheme)
    const htmlEl = document.querySelector('html')
    if (this.state.settings.useDarkTheme) {
      htmlEl.classList = 'dark'
    } else {
      htmlEl.classList = ''
    }
  }

  loadFromSaveCache(id) {
    return fetch('/save/cache/' + id, {
      method: 'get',
      headers: { 'Content-Type': 'application/json' }
    }).then(response => response.json())
      .then((data) => {
        this.setState({
          cacheId: window.tablisterJson,
          windows: data,
          savedWindows: null,
          savedWebPages: null
        })
      })
  }

  loadSavedWindows() {
    // tabListerTabLinks_savedLinks is a legacy name
    let loadedSavedWindows = store.get('tabListerTabLinks_savedLinks')

    if (typeof loadedSavedWindows === 'undefined') {
      return
    }

    this.setState({
      savedWindows: loadedSavedWindows,
      savedWindowsCount: loadedSavedWindows.wids.length
    })

    console.log('Load Saved Windows:', this.state)
  }

  loadSavedWebPages() {
    let loadedKeys = store.get('tabListerTabLinks_savedWebPageKeys')

    if (!loadedKeys) {
      return
    }

    let savedWebPages = {
      wids: loadedKeys.keys
    }

    this.setState({
      savedWebPages: savedWebPages,
      savedWebPagesCount: loadedKeys.keys.length
    })

    console.log('Load Saved Web Pages:', this.state)
  }

  captureWindowTabs(allWindowsBoolean, closeCapturedTabsBoolean) {
    if (allWindowsBoolean) {
      chrome.windows.getAll({ 'populate': true }, (windows) => {
        this.generateJson(windows, closeCapturedTabsBoolean)
      })
    } else {
      chrome.windows.getCurrent({ 'populate': true }, (win) => {
        let windows = []
        windows[0] = win

        this.generateJson(windows, closeCapturedTabsBoolean)
      })
    }
  }

  newTabObject(tab) {
    let temp = document.createElement('a')
    temp.href = tab.url

    let favicon = `https://www.google.com/s2/favicons?domain=${temp.hostname}`

    return {
      'favicon': favicon,
      'pinned': tab.pinned,
      'active': tab.active,
      'audible': tab.audible,
      'autoDiscardable': tab.autoDiscardable,
      'discarded': tab.discarded,
      'favIconUrl': tab.favIconUrl,
      'height': tab.height,
      'highlighted': tab.highlighted,
      'id': tab.id,
      'incognito': tab.incognito,
      'index': tab.index,
      'mutedInfo': tab.mutedInfo,
      'selected': tab.selected,
      'status': tab.status,
      'title': tab.title,
      'url': tab.url,
      'windowId': tab.windowId
    }
  }

  generateJson(windows, closeCapturedTabsBoolean) {
    let json = {}
    json.windows_links = {}
    json.wids = []

    let wid = new Date().getTime()

    windows.forEach(win => {
      wid = wid + 1

      json.wids.push(wid)
      json.windows_links[wid] = {}
      json.windows_links[wid].tab_links = []
      json.windows_links[wid].name = ''

      json.windows_links[wid].wid = wid
      json.windows_links[wid].id = win.id

      json.windows_links[wid].type = win.type
      json.windows_links[wid].incognito = win.incognito
      json.windows_links[wid].state = win.state

      json.windows_links[wid].top = win.top
      json.windows_links[wid].left = win.left
      json.windows_links[wid].width = win.width
      json.windows_links[wid].height = win.height

      let tabs = win.tabs

      tabs.forEach((tab, tabNumber) => {
        let thisTab = tabs[tabNumber]

        if (thisTab.url.startsWith('http')) {
          let newTabObject = this.newTabObject(thisTab)

          json.windows_links[wid].tab_links.push(newTabObject)

          if (closeCapturedTabsBoolean) {
            chrome.tabs.remove(tab.id)
          }
        }
      })
    })

    this.setState({
      windows: json,
      windowsCount: json.wids.length
    })

    console.log('Capture Open Windows:', this.state)

    // after capture runs; load saved windows, too
    this.loadSavedWindows()
    this.loadSavedWebPages()
  }

  // site / extension

  restoreAll() {
    // restoreAll needs to be slightly simpler for the saved webpage
    // ... so this check allows injected.js to override click event defined here
    if (typeof chrome.windows !== 'undefined' && typeof chrome.windows.create !== 'undefined') {
      document.querySelectorAll('li[data-window]').forEach(win => {
        let wid = win.getAttribute('data-window')

        let links = document.querySelectorAll(`[data-window='${wid}'] .tablink`)
        let urls = []
        links.forEach(link => {
          urls.push(link.getAttribute('href'))
        })

        let top = parseInt(win.getAttribute('data-top'), 10) || 0,
          left = parseInt(win.getAttribute('data-left'), 10) || 0,
          width = parseInt(win.getAttribute('data-width'), 10) || 0,
          height = parseInt(win.getAttribute('data-height'), 10) || 0,
          incognito = (win.getAttribute('data-incognito') == 'true') || false

        return chrome.windows.create({
          url: urls,
          top: top,
          left: left,
          width: width,
          height: height,
          incognito: incognito
        })
      })
    }
  }

  mailAll() {

    let message_body = this.exportAll(false)
    let message = message_body + '\n\n\n' + 'https://tablister.com' + '\n\n\n'
    let emailUrl = 'mailto:?subject=Tab%20Lister%20Export&body=' + encodeURIComponent(message)

    chrome.tabs.update({
      url: emailUrl
    });
  }

  exportAll(toggleDisplay) {
    let currentWindows = this.state.windows
    let savedWindows = this.state.savedWindows
    let exportField = document.getElementById('exportAll')
    let exportData = '', exportedWindowsData = '', exportedSavedWindowsData = ''

    if (currentWindows) {
      Object.keys(currentWindows.windows_links).forEach((window_key, index) => {
        currentWindows.windows_links[window_key].tab_links.forEach((tab, index) => {
          exportedWindowsData += tab.title + ':\n' + tab.url + '\n\n'
        })
      })

      if (exportedWindowsData.length > 0) {
        exportData += '\n\n' + exportedWindowsData
      }
    }

    if (savedWindows) {
      Object.keys(savedWindows.windows_links).forEach((window_key, index) => {
        savedWindows.windows_links[window_key].tab_links.forEach((tab, index) => {
          exportedSavedWindowsData += tab.title + ':\n' + tab.url + '\n\n'
        })
      })

      if (exportedSavedWindowsData.length > 0) {
        exportData += 'Saved Windows:' + '\n\n' + exportedSavedWindowsData
      }
    }

    console.log('exportAll', exportData)

    if (toggleDisplay) {
      exportField.value = exportData
      this.toggleExportAll()
    } else {
      return exportData
    }
  }

  copyExportAll() {
    new Clipboard('.copyExportAll')
  }

  toggleExportAll() {
    let el = document.getElementById('exportLinks')
    let bd = document.querySelector('body')
    if (el.style.display === 'none') {
      el.style.display = 'block'
      bd.style.overflowY = 'hidden'
    } else {
      el.style.display = 'none'
      bd.style.overflowY = 'auto'
    }
  }

  toggleSettings() {
    let el = document.getElementById('settings')
    let bd = document.querySelector('body')
    if (el.style.display === 'none') {
      el.style.display = 'block'
      bd.style.overflowY = 'hidden'
    } else {
      el.style.display = 'none'
      bd.style.overflowY = 'auto'
    }
  }

  // windows

  saveWindowLinks(wid) {
    console.log(`%csaveWindowLinks: ${wid}`, 'color: green')

    // TODO: this seems awkward for just testing & init'ing savedWindows
    let savedLinks = store.get('tabListerTabLinks_savedLinks') || 'undefined'
    if (savedLinks === 'undefined') {
      this.state.savedWindows = {}
      this.state.savedWindows.wids = []
      this.state.savedWindows.windows_links = {}
    }

    this.state.savedWindows.wids.push(wid)
    this.state.savedWindows.windows_links[wid.toString()] = this.state.windows.windows_links[wid.toString()]
    this.state.savedWindows.windows_links[wid.toString()]['saved_timestamp'] = (new Date()).getTime()

    let temp = this.state.savedWindows
    store.set('tabListerTabLinks_savedLinks', temp)

    this.setState({
      savedWindows: temp
    })

    this.loadSavedWindows()

    this.alert(`Saving this Window's Tabs`, 'info')
  }

  bookmarkWindowLinks(wid, type) {
    // console.log(`%cbookmarkWindowLinks, ${wid}, ${type}`, 'color: green')

    let folderName = 'Tab Lister bookmarks (' + wid + ')'

    let links = []

    if (type === 'saved') {
      links = this.state.savedWindows.windows_links[wid.toString()].tab_links
    } else {
      links = this.state.windows.windows_links[wid.toString()].tab_links
    }

    chrome.bookmarks.create({
      'title': folderName
    },
      function (newFolder) {
        // console.log('added folder: ', newFolder)

        links.forEach((link) => {
          // console.log('bookmark', link.title, link.url)
          console.log(`%cbookmarkWindowLinks, in ${folderName}: ${link.title}, ${link.url}`, 'color: green')

          chrome.bookmarks.create({
            'parentId': newFolder.id,
            'title': link.title,
            'url': link.url
          })
        })
      })

    this.alert(`Bookmarking Tabs, in folder ${folderName}`, 'info')
  }

  saveWindowToWebPage(wid) {
    let json = {}
    json.windows_links = {}
    json.wids = []

    json.windows_links[wid] = this.state.windows.windows_links[wid]
    json.wids = [wid]

    let data = new FormData()
    data.append('json', JSON.stringify(json))

    return fetch(this.state.config.saveBase, {
      method: 'post',
      body: data
    }).then((response) => response.json()).then((res) => {
      let key = res.key
      let url = this.state.config.viewBase + key + '/' + wid
      let webpages = store.get('tabListerTabLinks_savedWebPageKeys') || { 'keys': [] }

      if (wid !== null) {
        key = key + '/' + wid
      }

      webpages.keys.push(key)

      store.set('tabListerTabLinks_savedWebPageKeys', webpages)

      this.setState({
        savedWebPages: webpages,
        savedWebPagesCount: webpages.keys.length
      })

      this.loadSavedWebPages()

      console.log(`%csaveWindowToWebpage, ${url}`, 'color: green')

      this.alert(`Web page created: `, 'info', url)
    })
  }

  restoreWindowLinks(wid) {
    let links = document.querySelectorAll(`[data-window='${wid}'] .tablink`)
    let urls = []
    links.forEach(link => {
      urls.push(link.getAttribute('href'))
    })

    let win = document.querySelector(`[data-window='${wid}']`)

    let top = parseInt(win.getAttribute('data-top'), 10) || 0,
      left = parseInt(win.getAttribute('data-left'), 10) || 0,
      width = parseInt(win.getAttribute('data-width'), 10) || 0,
      height = parseInt(win.getAttribute('data-height'), 10) || 0,
      incognito = (win.getAttribute('data-incognito') == 'true') || false

    return chrome.windows.create({
      url: urls,
      top: top,
      left: left,
      width: width,
      height: height,
      incognito: incognito
    })
  }

  removeWindowLinks(wid, type) {
    // type here is 'saved' or 'captured'
    let temp_windows = this.state.windows

    if (type === 'saved') {
      temp_windows = this.state.savedWindows
    }

    console.log('removeWindowLinks', wid, type, temp_windows)

    // remove windows_links
    delete temp_windows.windows_links[wid]
    // remove wid from wids
    temp_windows.wids = temp_windows.wids.filter(e => e !== wid)

    if (type === 'saved') {

      this.setState({
        savedWindows: temp_windows,
        savedWindowsCount: temp_windows.wids.length
      })

      store.set('tabListerTabLinks_savedLinks', temp_windows)
    } else {
      this.setState({
        windows: temp_windows,
        windowsCount: temp_windows.wids.length
      })
    }

    console.log('removeWindowLinks', wid, type, this.state)

    console.log(`%cremoveWindowLinks, ${wid} (${type})`, 'color: pink')

    this.alert('Window removed', 'success')
  }

  // tab

  openTabLink(e, tab, wid, context) {
    // console.log('openTabLink', tab, wid, context)

    e.preventDefault()

    chrome.tabs.get(tab.id, function (getTabResult) {
      // console.log('get tab', getTabResult, tab, wid, context)

      if (getTabResult) {
        chrome.windows.update(tab.windowId, { focused: true })
        chrome.tabs.update(tab.id, { selected: true })
      } else {
        chrome.tabs.create({ url: tab.url })
      }
    })
  }

  removeTabLink(tab, wid, context) {

    // BUG: when removing last tab ... windows should also get programmtically removed!!!

    // context here is 'saved' or 'captured'
    let temp_windows = this.state.windows

    if (context === 'saved') {
      temp_windows = this.state.savedWindows
    }

    console.log('removeTabLink', wid, context, temp_windows)

    Object.keys(temp_windows.windows_links).forEach((key, index) => {
      let window_key = key

      temp_windows.windows_links[window_key].tab_links.forEach((key, index) => {
        if (_.isEqual(temp_windows.windows_links[window_key].tab_links[index], tab)) {
          temp_windows.windows_links[window_key].tab_links.splice(index, 1)
        }
      })
    })

    if (context === 'saved') {

      this.setState({
        savedWindows: temp_windows,
        savedWindowsCount: temp_windows.wids.length
      })

      store.set('tabListerTabLinks_savedLinks', temp_windows)
    } else {
      this.setState({
        windows: temp_windows,
        windowsCount: temp_windows.wids.length
      })
    }

    console.log('removeTabLink', wid, context, this.state)

    console.log(`%cremoveTabLink, ${tab}`, 'color: pink')

    this.alert('Tab removed', 'success')
  }

  closeTab(id) {
    // console.log('closeTab', id)
    console.log(`%ccloseTab, ${id}`, 'color: pink')

    document.getElementById(`close-tab-${id}`).setAttribute('disabled', true)

    chrome.tabs.remove(id)
  }

  closeWindow(id) {
    // console.log('closeWindow', id)
    console.log(`%ccloseWindow, ${id}`, 'color: pink')

    document.getElementById(`close-window-${id}`).setAttribute('disabled', true)

    chrome.windows.getCurrent({ 'populate': true }, (win) => {
      if (id != win.id) {
        // not current window:
        chrome.windows.remove(id)
      } else {
        // this is current window:
        chrome.tabs.getCurrent(function (tab) {
          const currentTabId = tab.id

          // don't close current tab
          win.tabs.forEach(function (tab) {
            if (tab.id !== currentTabId) {
              chrome.tabs.remove(tab.id)
            }
          })
        })
      }
    })
  }

  toggleWindow(id, newState) {
    // console.log('toggleWindow', id, newState)
    console.log(`%ctoggleWindow, ${id}, (${newState})`, (newState === 'minimized' ? 'color: pink' : 'color: green'))

    if (newState === 'minimized') {
      chrome.windows.update(id, { state: 'minimized' })
    } else {
      chrome.windows.update(id, { state: 'normal', focused: true })
    }
  }

  highlight(id, target) {
    let tl = document.getElementById('Tablister')
    let wn = document.getElementById(`window-${id}`)

    if (id === 'all-windows') {
      tl.setAttribute(`data-highlight-${id}-${target}`, true)
      tl.setAttribute(`data-highlighter`, true)
    } else {
      wn.setAttribute(`data-highlight-${target}`, true)
      wn.setAttribute(`data-highlighter`, true)
    }
  }

  unHighlight(id, target) {
    let tl = document.getElementById('Tablister')
    let wn = document.getElementById(`window-${id}`)

    if (id === 'all-windows') {
      tl.removeAttribute(`data-highlight-${id}-${target}`)
      tl.removeAttribute(`data-highlighter`)
    } else {
      wn.removeAttribute(`data-highlight-${target}`)
      wn.removeAttribute(`data-highlighter`)
    }
  }

  alert(message, type, url, duration = 8) {
    this.setState({
      message: message,
      messageType: type,
      messageUrl: url
    }, () => {
      document.querySelector('.alert-banner').classList = 'alert-banner alert-banner-open'

      if (type !== 'error') {
        setTimeout(() => {
          document.querySelector('.alert-banner').classList = 'alert-banner'
        }, (duration * 1000))
      }
    })
  }

  report() {

    if (this.state.cacheId) {
      return
    }

    // this function updates the UI with information from watchWindows

    chrome.windows.getCurrent((currentWindow) => {
      chrome.windows.getAll({ 'populate': true }, (windows) => {
        windows.forEach(win => {
          // console.log('focus changed', win.id, '===>>>', win.state)
          // window could be under both captured & saved so ...
          let winInstances = document.querySelectorAll(`li[class="windows_links"][data-id="${win.id}"]`)

          if (winInstances) {
            winInstances.forEach((winInstance) => {
              winInstance.setAttribute('data-state', win.state)

              if (win.id === currentWindow.id) {
                winInstance.setAttribute('data-current-window', true)
              }
            })
          }
        })
      })
    })
  }

  watchWindows() {

    if (this.state.cacheId) {
      return
    }

    // this function monitors ec=vents emitted by a browsers' windows & tabs

    this.report()

    chrome.windows.onFocusChanged.addListener((wid) => {
      this.report()
    })

    chrome.windows.onRemoved.addListener((windowId) => {
      console.log(`%cwindow closed: ${windowId}`, 'color: pink')

      let winInstances = document.querySelectorAll(`li[class="windows_links"][data-id="${windowId}"]`)

      if (winInstances) {
        winInstances.forEach((winInstance) => {
          winInstance.setAttribute('data-window-closed', true)
        })
      }

      this.report()
    })

    chrome.tabs.onCreated.addListener((tab) => {
      console.log(`%cchrome.tabs.onCreated: ${tab.id} in  ${tab.windowId}`, 'color: green')

      if (tab.url === this.state.environment.newTabUrl) {
        if (this.state.settings.replaceNewTab) {
          chrome.tabs.update(tab.id, {
            url: chrome.extension.getURL('display-newtab.html')
          })
        }
      }

      let newTabId = tab.id

      chrome.tabs.onUpdated.addListener((tabId, changeInfo, tab) => {

        if (tabId === newTabId && changeInfo.status === 'complete') {

          let json = this.state.windows
          let thisTab = tab
          let widSelector = document.querySelector(`.captured li[data-id="${thisTab.windowId}"]`)
          let wid = (widSelector !== null) ? widSelector.getAttribute('data-window') : null

          // console.log(`%cchrome.tabs.onUpdated: ${tab.id} in  ${tab.windowId} & ${changeInfo.status}`, 'color: green')
          // console.log('check new tab to see if wid exists', widSelector, wid)

          if (thisTab.url.startsWith('http')) {
            let newTabObject = this.newTabObject(thisTab)

            if (wid === null) {
              wid = new Date().getTime()

              json.wids.push(wid)

              json.windows_links[wid] = {}
              json.windows_links[wid].tab_links = []
              json.windows_links[wid].name = ''

              json.windows_links[wid].wid = wid

              json.windows_links[wid].id = thisTab.windowId // win.id

              chrome.windows.get(thisTab.windowId, (win) => {

                json.windows_links[wid].type = win.type
                json.windows_links[wid].incognito = win.incognito
                json.windows_links[wid].state = win.state

                json.windows_links[wid].top = win.top
                json.windows_links[wid].left = win.left
                json.windows_links[wid].width = win.width
                json.windows_links[wid].height = win.height

                json.windows_links[wid].tab_links.push(newTabObject)
                this.setState({ windows: json })
              })
            } else {
              json.windows_links[wid].tab_links.push(newTabObject)
              this.setState({ windows: json })
            }

            setTimeout(() => {
              window.f.dispatchEvent('settle')
            }, 200)
          }
        }
      })
    })

    chrome.tabs.onRemoved.addListener((tabId) => {
      console.log(`%ctab closed: ${tabId}`, 'color: pink')

      // need to hide window close & min/max icons

      let tabInstances = document.querySelectorAll(`li[data-tab-id="${tabId}"]`)

      if (tabInstances) {
        tabInstances.forEach((tabInstance) => {
          tabInstance.setAttribute('data-tab-closed', true)
        })
      }

      this.report()
    })
  }

  debounce(func, wait, immediate) {
    // console.log('debounce', func, wait, immediate)
    let timeout
    return function () {
      let context = this
      let args = arguments
      let later = function () {
        timeout = null
        if (!immediate) func.apply(context, args)
      }
      let callNow = immediate && !timeout
      clearTimeout(timeout)
      timeout = setTimeout(later, wait)
      if (callNow) func.apply(context, args)
    }
  }

  render() {
    return (
      <div id='Tablister'>
        <AlertBanner {...this.state} />
        <SiteToolbar
          pluginUrl={this.state.pluginUrl}
          restoreAll={this.restoreAll}
          mailAll={this.mailAll.bind(this)}
          exportAll={this.exportAll.bind(this)}
          toggleSettings={this.toggleSettings.bind(this)}
          highlight={this.highlight.bind(this)}
          unHighlight={this.unHighlight.bind(this)} />
        <Settings
          {...this.state}
          updateSettings={this.updateSettings.bind(this)}
          toggleSettings={this.toggleSettings.bind(this)} />
        <ExportTabs toggleExportAll={this.toggleExportAll.bind(this)} />
        <div className='carousel'>
          <div className="carousel-cell">
            <WindowsCaptured
              tabClassName='captured'
              idAttribute='linkHtml'
              classNames='current captured'

              {...this.state}
              windows={this.state.windows}

              saveWindowLinks={this.saveWindowLinks.bind(this)}
              bookmarkWindowLinks={this.bookmarkWindowLinks.bind(this)}
              saveWindowToWebPage={this.saveWindowToWebPage.bind(this)}
              restoreWindowLinks={this.restoreWindowLinks.bind(this)}
              removeWindowLinks={this.removeWindowLinks.bind(this)}
              closeWindow={this.closeWindow}
              toggleWindow={this.toggleWindow}
              report={this.report}
              openTabLink={this.openTabLink.bind(this)}
              removeTabLink={this.removeTabLink.bind(this)}
              closeTab={this.closeTab}
              highlight={this.highlight.bind(this)}
              unHighlight={this.unHighlight.bind(this)} />
          </div>
          <div className="carousel-cell">
            <WindowsSaved
              tabClassName='saved'
              idAttribute='savedLinkHtml'
              classNames='saved'

              {...this.state}
              windows={this.state.savedWindows}

              saveWindowToWebPage={this.saveWindowToWebPage.bind(this)}
              bookmarkWindowLinks={this.bookmarkWindowLinks.bind(this)}
              restoreWindowLinks={this.restoreWindowLinks.bind(this)}
              removeWindowLinks={this.removeWindowLinks.bind(this)}
              closeWindow={this.closeWindow}
              toggleWindow={this.toggleWindow}
              report={this.report}
              openTabLink={this.openTabLink.bind(this)}
              removeTabLink={this.removeTabLink.bind(this)}
              closeTab={this.closeTab}
              highlight={this.highlight.bind(this)}
              unHighlight={this.unHighlight.bind(this)} />
          </div>
          <div className="carousel-cell">
            <WindowsSavedWebPages
              tabClassName='saved'
              idAttribute='savedWebPagesHtml'
              classNames='saved-webpages'
              {...this.state} />
          </div>
        </div>
      </div>
    )
  }
}

ReactDOM.render(<TablisterApp />, document.querySelector('.container'))
