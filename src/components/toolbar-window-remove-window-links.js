import React from 'react'

const RemoveWindowLinks = (props) => {

  const { wid, type, highlight, unHighlight, removeWindowLinks } = props

  return (
    <a
      onClick={() => removeWindowLinks(wid, type)}
      onMouseEnter={() => highlight(wid, 'removeWindowLinks')}
      onMouseLeave={() => unHighlight(wid, 'removeWindowLinks')}
      className='removeWindowLinks hoverLink ion-ios-minus-outline'>
      <span>remove from this list</span>
    </a>
  )
}

export default RemoveWindowLinks
