import React from 'react'

import CreateWebPage from './toolbar-window-create-webpage'
import SaveThisWindowsTabs from './toolbar-window-save-this-windows-tabs'
import SaveToBookmarks from './toolbar-window-save-to-bookmarks'
import RestoreWindowLinks from './toolbar-window-restore-window-links'
import RemoveWindowLinks from './toolbar-window-remove-window-links'
import CloseWindow from './toolbar-window-close-window'
import ToggleWindow from './toolbar-window-toggle-window'

const ToolbarForWindow = (props) => {

  const { wid, id, type, highlight, unHighlight,
    bookmarkWindowLinks,
    saveWindowLinks,
    config,
    saveWindowToWebPage,
    restoreWindowLinks,
    removeWindowLinks,
    closeWindow,
    toggleWindow,
    report } = props

  return (
    <div className='toolbar toolbar-window'>
      <SaveToBookmarks
        wid={wid}
        type={type}
        bookmarkWindowLinks={bookmarkWindowLinks}
        highlight={highlight}
        unHighlight={unHighlight} />
      <SaveThisWindowsTabs
        wid={wid}
        saveWindowLinks={saveWindowLinks}
        highlight={highlight}
        unHighlight={unHighlight} />
      <CreateWebPage
        config={config}
        wid={wid}
        saveWindowToWebPage={saveWindowToWebPage}
        highlight={highlight}
        unHighlight={unHighlight} />
      <RestoreWindowLinks
        wid={wid}
        restoreWindowLinks={restoreWindowLinks}
        highlight={highlight}
        unHighlight={unHighlight} />
      <RemoveWindowLinks
        wid={wid}
        type={type}
        removeWindowLinks={removeWindowLinks}
        highlight={highlight}
        unHighlight={unHighlight} />
      <CloseWindow id={id} closeWindow={closeWindow} />
      <ToggleWindow id={id} toggleWindow={toggleWindow}
        report={report} />
    </div>
  )
}

export default ToolbarForWindow
