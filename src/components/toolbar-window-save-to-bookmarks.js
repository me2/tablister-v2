import React from 'react'

const SaveToBookmarks = (props) => {

  const { wid, type, highlight, unHighlight, bookmarkWindowLinks } = props

  return (
    <a
      onClick={() => bookmarkWindowLinks(wid, type)}
      onMouseEnter={() => highlight(wid, 'bookmarkWindowLinks')}
      onMouseLeave={() => unHighlight(wid, 'bookmarkWindowLinks')}
      className='bookmarkWindowLinks hoverLink ion-ios-star-outline'>
      <span>save to bookmarks</span>
    </a>
  )
}

export default SaveToBookmarks
