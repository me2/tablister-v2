import React, { Component } from 'react'
import Moment from 'moment'
import WindowToolbar from './toolbar-window'
import Tab from './tab'

class WindowsSaved extends Component {

  render() {

    const { windows, tabClassName, idAttribute, classNames } = this.props

    try {

      if (windows.wids === null) {
        return (
          <div className={tabClassName}>
            <h3>No Saved Windows</h3>
          </div>
        )
      }

      const wins = windows.wids.map(wid => {
        const win = windows.windows_links[wid]
        const window_id_attribute = `window-${wid}`
        const key = `saved-${wid}`

        if (!win || typeof win === 'undefined' || !win.tab_links || win.tab_links.length < 1) {
          return
        }

        const timestamp = win.saved_timestamp || wid
        const formatted_timestamp = (timestamp === '') ? '' : Moment(parseInt(timestamp, 10)).format('dddd, MMMM Do YYYY, h:mm a')

        const tabs = win.tab_links.map(tab => {
          if (!tab) {
            return
          }

          let key = `saved-tab-${wid}-${tab.id}`

          return (<Tab key={key} context='saved' wid={wid} tab={tab} {...this.props} />)
        })

        return (
          <li
            className='windows_links'
            id={window_id_attribute}
            data-saved={timestamp}
            key={key}
            data-window={wid}
            data-id={win.id}>
            <h4>
              <span className='viewWindowName' />
              <span className='total'>Total: {win.tab_links.length} links</span>
              <WindowToolbar
                type='saved'
                wid={wid}
                {...this.props}
              />
              <span className='saved-timestamp'>{formatted_timestamp}</span>
            </h4>
            <ol>
              {tabs}
            </ol>
          </li>
        )
      })

      if (wins.length === 0) {
        return (
          <div className={tabClassName}>
            <h3>No Saved Windows</h3>
          </div>
        )
      }

      return (
        <div className={tabClassName}>
          <ol id={idAttribute} className={classNames}>
            {wins}
          </ol>
        </div>
      )

    } catch (e) {

      // console.log(`%cWindowsSaved, none, ${e})`, 'color: pink')

      return (
        <div className={tabClassName}>
          <h3>No Saved Windows</h3>
        </div>
      )

    }
  }
}

export default WindowsSaved
