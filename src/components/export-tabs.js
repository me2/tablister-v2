import React from 'react'

const ExportTabs = (props) => {

  const { toggleExportAll } = props

  return (
    <form id='exportLinks' style={{ display: `none` }}>
      <fieldset>
        <a className='copyExportAll' data-clipboard-target='#exportAll'>copy text below:</a>
        <a onClick={() => toggleExportAll()} className='toggleExportAll ion-ios-close-outline'>close</a>
        <textarea id='exportAll' />
      </fieldset>
    </form>
  )
}

export default ExportTabs
