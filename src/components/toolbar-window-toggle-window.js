import React from 'react'

const ToggleWindow = (props) => {

  const { id, toggleWindow, report } = props

  return <span>
    <a onClick={() => {
      toggleWindow(id, "minimized")
      report()
    }} className="hoverLink maximize ion-ios-minus">
      <span className="current">minimize this (current) window</span>
      <span className="generic">minimize this window</span>
    </a>
    <a onClick={() => {
      toggleWindow(id, "normal")
      report()
    }} className="hoverLink minimize ion-ios-plus">
      <span className="current">focus on this (current) window</span>
      <span className="generic">focus on this window</span>
    </a>
  </span>;
}

export default ToggleWindow
