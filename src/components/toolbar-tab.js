import React from 'react'

import RemoveTabLink from './toolbar-tab-remove-window-link'
import CloseTab from './toolbar-tab-close-tab'

const ToolbarForTab = (props) => {

  const { tab, wid, context, id, removeTabLink, highlight, unHighlight, closeTab } = props

  return (
    <div className='toolbar toolbar-tab'>
      <RemoveTabLink
        tab={tab}
        wid={wid}
        context={context}
        removeTabLink={removeTabLink}
        highlight={highlight}
        unHighlight={unHighlight} />
      <CloseTab id={id} closeTab={closeTab} />
    </div>
  )
}

export default ToolbarForTab
