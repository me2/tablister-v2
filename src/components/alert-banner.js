import React from 'react'

const AlertBanner = (props) => {

  const { message, messageType, messageUrl } = props

  return (
    <div className='alert-banner' data-alert-banner={messageType}>
      <section>
        {message}
        <a target='_new' href={messageUrl}>
          {messageUrl}
        </a>
      </section>
    </div>
  )
}

export default AlertBanner
