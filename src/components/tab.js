
import React from 'react'

import TabToolbar from './toolbar-tab'

const Tab = (props) => {

  const { tab, key, wid, openTabLink, removeTabLink, closeTab, context } = props

  return (
    <li key={key} data-tab-id={tab.id} data-pinned={tab.pinned} data-incognito={tab.incognito}>
      <span>
        <a
          onClick={(e) => openTabLink(e, tab, wid, context)}
          href={tab.url} target='_new' className='tablink'>
          <img className='favicon' src={tab.favicon} />
          <span className='title'>{tab.title}</span>
        </a>
        <TabToolbar tab={tab} wid={wid} id={tab.id} removeTabLink={removeTabLink} closeTab={closeTab} context={context} />
      </span>
    </li>
  )
}

export default Tab
