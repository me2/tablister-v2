import React from 'react'

const ToggleSettings = (props) => {

  const { toggleSettings } = props

  return (
    <a
      onClick={() => toggleSettings()}
      className='toggleSettings ion-ios-gear-outline'>
      <span>settings</span>
    </a>
  )
}

export default ToggleSettings
