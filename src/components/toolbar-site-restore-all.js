import React from 'react'

const RestoreAll = (props) => {

  const { highlight, unHighlight, restoreAll } = props

  return (
    <a
      onClick={() => restoreAll()}
      onMouseEnter={() => highlight('all-windows', 'restoreAll')}
      onMouseLeave={() => unHighlight('all-windows', 'restoreAll')}
      className='restoreAll ion-ios-paper-outline'>
      <span>restore all links</span>
    </a>
  )
}

export default RestoreAll
