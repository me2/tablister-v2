import React from 'react'

const RemoveTabLink = (props) => {

  const { tab, wid, context, removeTabLink } = props

  return (
    <a
      onClick={() => removeTabLink(tab, wid, context)}
      className='removeTabLink hoverLink ion-ios-minus-outline'>
      <span>remove from list</span>
    </a>
  )
}

export default RemoveTabLink
