import React from 'react'
import Moment from 'moment'
import store from 'store'

const WindowsSavedWebPages = (props) => {

  const { config, tabClassName, idAttribute, classNames } = props
  const savedWebPagesKeys = store.get('tabListerTabLinks_savedWebPageKeys')

  try {

    const savedWebPages = savedWebPagesKeys.keys.map(savedWebPagesKey => {
      const savedWebPagesUrl = config.viewBase + savedWebPagesKey
      const temp = savedWebPagesKey.split('/')
      const timestamp = Moment(parseInt(temp[1], 10)).format('dddd, MMMM Do YYYY, h:mm a')

      if (timestamp == 'Invalid date') {
        return
      }

      return (<li key={savedWebPagesKey}><a href={savedWebPagesUrl} target='_blank'>{timestamp}</a></li>)
    })

    if (savedWebPages.length === 0) {
      return (
        <div className={tabClassName}>
          <h3>No Saved Web Pages</h3>
        </div>
      )
    }

    return (
      <div className={tabClassName}>
        <ol id={idAttribute} className={classNames}>
          {savedWebPages}
        </ol>
      </div>
    )

  } catch (e) {

    // console.log(`%cWindowsSavedWebPages, none, ${e})`, 'color: pink')

    return (
      <div className={tabClassName}>
        <h3>No Saved Web Pages</h3>
      </div>
    )

  }
}

export default WindowsSavedWebPages
