import React from 'react'

const MailAll = (props) => {

  const { highlight, unHighlight, mailAll } = props

  return (
    <a
      onClick={() => mailAll()}
      onMouseEnter={() => highlight('all-windows', 'mailAll')}
      onMouseLeave={() => unHighlight('all-windows', 'mailAll')}
      className='mailAll ion-ios-email-outline'>
      <span>email all links</span>
    </a>
  )
}

export default MailAll
