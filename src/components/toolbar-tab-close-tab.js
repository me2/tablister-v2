import React from 'react'

const CloseTab = (props) => {

  const { id, closeTab } = props
  const idAttr = `close-tab-${id}`

  return (
    <a
      onClick={() => closeTab(id)}
      id={idAttr}
      className='closeTab hoverLink ion-ios-close-outline'>
      <span>close tab</span>
    </a>
  )
}

export default CloseTab
