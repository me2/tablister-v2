import React from 'react'

const CloseWindow = (props) => {

  const { id, closeWindow } = props
  const idAttr = `close-window-${id}`

  return (
    <a
      onClick={() => closeWindow(id)}
      id={idAttr}
      className='closeWindow hoverLink ion-ios-close-outline'>
      <span>close window</span>
    </a>
  )
}

export default CloseWindow
