import React from 'react'

const ExportAll = (props) => {

  const { highlight, unHighlight, exportAll } = props

  return (
    <a
      onClick={() => exportAll(true)}
      onMouseEnter={() => highlight('all-windows', 'exportAll')}
      onMouseLeave={() => unHighlight('all-windows', 'exportAll')}
      className='exportAll ion-ios-paperplane-outline'>
      <span>export all links</span>
    </a>
  )
}

export default ExportAll
