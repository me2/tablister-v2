import React from 'react'

const RestoreWindowLinks = (props) => {

  const { wid, highlight, unHighlight, restoreWindowLinks } = props

  return (
    <a
      onClick={() => restoreWindowLinks(wid)}
      onMouseEnter={() => highlight(wid, 'restoreWindowLinks')}
      onMouseLeave={() => unHighlight(wid, 'restoreWindowLinks')}
      className='restoreWindowLinks hoverLink ion-ios-paper-outline'>
      <span>restore links</span>
    </a>
  )
}

export default RestoreWindowLinks
