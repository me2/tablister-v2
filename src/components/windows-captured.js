import React, { Component } from 'react'
import Moment from 'moment'
import WindowToolbar from './toolbar-window'
import Tab from './tab'

class WindowsCaptured extends Component {

  render() {

    const { windows, tabClassName, idAttribute, classNames } = this.props

    if (!windows) {
      return (<div className='TablisterLoading'><span className='ion-load-c' />Loading&#8230;</div>)
    }

    const wins = windows.wids.map(wid => {
      const win = windows.windows_links[wid]
      const window_id_attribute = `window-${wid}`
      let key = `captured-${wid}`

      if (!win || typeof win === 'undefined' || !win.tab_links || win.tab_links.length < 1) {
        return
      }

      const timestamp = win.saved_timestamp || wid
      const formatted_timestamp = (timestamp === '') ? '' : Moment(parseInt(timestamp, 10)).format('dddd, MMMM Do YYYY, h:mm a')

      const tabs = win.tab_links.map(tab => {
        if (!tab) {
          return
        }

        let key = `captured-tab-${wid}-${tab.id}`

        return (<Tab key={key} context='captured' wid={wid} tab={tab} {...this.props} />)
      })

      return (
        <li
          className='windows_links'
          key={key}
          id={window_id_attribute}
          data-window={wid}
          data-saved={timestamp}
          data-id={win.id}
          data-top={win.top}
          data-left={win.left}
          data-width={win.width}
          data-height={win.height}
          data-incognito={win.incognito}>
          <h4>
            <span className='viewWindowName' />
            <span className='total'>Total: {win.tab_links.length} links</span>
            <WindowToolbar
              type='captured'
              wid={wid}
              id={win.id}
              {...this.props}
            />
            <span className='saved-timestamp'>{formatted_timestamp}</span>
          </h4>
          <ol>
            {tabs}
          </ol>
        </li>
      )
    })

    return (
      <div className={tabClassName}>
        <ol id={idAttribute} className={classNames}>
          {wins}
        </ol>
      </div>
    )
  }
}

export default WindowsCaptured
