import React from 'react'
import store from 'store'

const CreateWebPage = (props) => {

  const { wid, highlight, unHighlight, saveWindowToWebPage } = props
  const savedWebPagesKeys = store.get('tabListerTabLinks_savedWebPageKeys')

  let savedWebPagesUrl = ''

  if (savedWebPagesKeys && savedWebPagesKeys.keys) {
    savedWebPagesKeys.keys.map(savedWebPagesKey => {
      let temp = savedWebPagesKey.split('/')

      if (parseInt(temp[1], 10) === parseInt(wid, 10)) {
        savedWebPagesUrl = `https://tablister.com/view-v2/${savedWebPagesKey}`
      }
    })
  }

  if (savedWebPagesUrl !== '') {
    return (
      <a
        onMouseEnter={() => highlight(wid, 'saveWindowToWebPage')}
        onMouseLeave={() => unHighlight(wid, 'saveWindowToWebPage')}
        href={savedWebPagesUrl}
        target='_new'
        className='saveWindowToWebPage hoverLink ion-ios-cloud-upload-outline'>
        <span>open web page</span>
      </a>
    )
  }

  return (
    <a
      onClick={() => saveWindowToWebPage(wid)}
      onMouseEnter={() => highlight(wid, 'saveWindowToWebPage')}
      onMouseLeave={() => unHighlight(wid, 'saveWindowToWebPage')}
      className='saveWindowToWebPage hoverLink ion-ios-cloud-upload-outline'>
      <span>create web page</span>
    </a>
  )
}

export default CreateWebPage
