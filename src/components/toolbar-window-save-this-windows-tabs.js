import React from 'react'

const SaveThisWindowsTabs = (props) => {

  const { wid, highlight, unHighlight, saveWindowLinks } = props

  return (
    <a
      onClick={() => saveWindowLinks(wid)}
      onMouseEnter={() => highlight(wid, 'saveWindowLinks')}
      onMouseLeave={() => unHighlight(wid, 'saveWindowLinks')}
      className='saveWindowLinks hoverLink ion-ios-download-outline'>
      <span>save this window's tabs</span>
    </a>
  )
}

export default SaveThisWindowsTabs
