import React, { Component } from 'react'

import RestoreAll from './toolbar-site-restore-all'
import MailAll from './toolbar-site-mail-all'
import ExportAll from './toolbar-site-export-all'
import ToggleSettings from './toolbar-site-toggle-settings'

class ToolbarForSite extends Component {

  render() {

    const { pluginUrl, restoreAll, mailAll, exportAll, toggleSettings, highlight, unHighlight } = this.props
    const { windowsCount, savedWindowsCount, savedWebPagesCount } = this.props
    const pluginFullPageUrl = pluginUrl.replace('-popup', '')

    return (
      <div className='toolbar toolbar-site'>

        <div className='toolbar-buttons'>
          <button onClick={() => window.f.select(0)} className='selected'>
            {windowsCount} Captured Windows
          </button>
          <button onClick={() => window.f.select(1)}>
            {savedWindowsCount} Saved Windows
          </button>
          <button onClick={() => window.f.select(2)}>
            {savedWebPagesCount} Saved Web Pages
          </button>
        </div>

        <div className='toolbar-links'>

          <a className='fullpage-link' href={pluginFullPageUrl} target='_blank'>View as Full Page</a>

          <RestoreAll
            restoreAll={restoreAll}
            highlight={highlight}
            unHighlight={unHighlight} />
          <MailAll
            mailAll={mailAll}
            highlight={highlight}
            unHighlight={unHighlight} />
          <ExportAll
            exportAll={exportAll}
            highlight={highlight}
            unHighlight={unHighlight} />
          <ToggleSettings
            toggleSettings={toggleSettings}
            highlight={highlight}
            unHighlight={unHighlight} />
        </div>
      </div>
    )
  }
}

export default ToolbarForSite
