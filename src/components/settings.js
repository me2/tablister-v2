import React from 'react'

const Settings = (props) => {

  const { settings, toggleSettings, updateSettings } = props

  const checked = 'ion-android-checkbox-outline'
  const unchecked = 'ion-android-checkbox-outline-blank'

  const applyToAllWindowsClassName = settings.applyToAllWindows ? checked : unchecked
  const closeTabsClassName = settings.closeTabs ? checked : unchecked
  const useDarkThemeClassName = settings.useDarkTheme ? checked : unchecked
  const replaceNewTabClassName = settings.replaceNewTab ? checked : unchecked

  return (
    <div className='settings' id='settings' style={{ display: 'none' }}>
      <fieldset>
        <h2>settings:</h2>
        <a onClick={() => toggleSettings()} className='toggleSettings ion-ios-close-outline'>close</a>
        <hr />
        <ul>
          <li>
            <label id='apply-to-all-windows' onClick={() => updateSettings('apply-to-all-windows')} className={applyToAllWindowsClassName}>
              If checked, Tab Lister creates lists of tabs from all windows
            </label>
          </li>
          <li>
            <label id='close-tabs' onClick={() => updateSettings('close-tabs')} className={closeTabsClassName}>
              If checked, Tab Lister will close all windows and tabs (except this one)
            </label>
          </li>
        </ul>
        <p>
          Note: checking the first setting gathers all windows into a single list &amp; and closing the windows or tabs can still be done without
          changing the second setting ... Test the usage of this extension to see how those behave.
        </p>
        <hr />
        <ul>
          <li>
            <label id='use-dark-theme' onClick={() => updateSettings('use-dark-theme')} className={useDarkThemeClassName}>
              If checked, Tab Lister will look darker
            </label>
          </li>
          <li>
            <label id='replace-newtab' onClick={() => updateSettings('replace-newtab')} className={replaceNewTabClassName}>
              If checked, Tab Lister will replace the New Tab experience (and opening a new tab or window will start with the Tab Lister page).
            </label>
          </li>
        </ul>
        <hr />
      </fieldset>
    </div>
  )
}

export default Settings
