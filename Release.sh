#!/bin/bash

grunt

cd dist/ && zip -r tab_lister_2.0-$1.chrome.zip tablister.web-ext && cd ../ \
    && mv dist/tab_lister_2.0-$1.chrome.zip ./tab_lister_2.0-$1.chrome.zip

cd dist/tablister.web-ext && web-ext build && cd ../../ \
    && mv dist/tablister.web-ext/web-ext-artifacts/tab_lister_2.0-$1.zip ./tab_lister_2.0-$1.firefox.zip

grunt review && rm -r dist/tablister.web-ext/web-ext-artifacts/ && zip -r tab_lister_2.0-$1.review.zip review
