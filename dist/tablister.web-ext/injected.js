var restoreAll = document.querySelector('.restoreAll')

if (restoreAll) {
  restoreAll.addEventListener('click', function (event) {
    // console.log('injected', '.restoreAll')

    let winCount = 0
    document.querySelectorAll('.tablink').forEach(tablink => {
      winCount = winCount + 1
      // console.log(tablink.getAttribute('href'), '_restoreAll_' + winCount)
      return window.open(tablink.getAttribute('href'), '_blank')
    })
  })
}

var notInstalled = document.querySelector('.not-installed')

if (notInstalled) {
  notInstalled.innerHTML = 'Thanks for using Tablister!!'
  notInstalled.classList = 'get-tablister installed'
}
